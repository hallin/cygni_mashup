package se.hallindesign.cygni.mashup;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ArtistController {

	private ArtistService service;

	public ArtistController(ArtistService service) {
		this.service = service;
	}

	@GetMapping("/artist/{mbid}")
	public Artist artist(@PathVariable String mbid) {
		return service.getArtist(mbid);
	}
}
