package se.hallindesign.cygni.mashup.consumers.responses.musicbrainz;

public class Relations {

	private String type;
	private URL url;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public URL getUrl() {
		return url;
	}

	public void setUrl(URL url) {
		this.url = url;
	}
}
