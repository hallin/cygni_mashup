package se.hallindesign.cygni.mashup.consumers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;

public class WikipediaConsumer {

	private final String URL_PREFIX = "https://en.wikipedia.org/w/api.php?action=query&format=json&prop=extracts&exintro=true&redirects=true&titles=";
	private JsonNode responseNode;

	private static final Logger LOGGER = LoggerFactory.getLogger(WikipediaConsumer.class);

	public WikipediaConsumer(String title) {
		RestTemplate restTemplate = new RestTemplate();
		responseNode = restTemplate.getForObject(URL_PREFIX + title, JsonNode.class);
		LOGGER.debug("JSON response = \n" + responseNode);
	}

	public String getExtract() {
		if (responseNode == null) {
			LOGGER.debug("getExtract() response is null, returning empty string");
			return "";
		}
		JsonNode extract = responseNode.findPath("extract");
		LOGGER.debug("getExtract() extract: " + extract);
		return extract.asText();
	}

}
