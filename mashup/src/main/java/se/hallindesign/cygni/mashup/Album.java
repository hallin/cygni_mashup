package se.hallindesign.cygni.mashup;

public class Album {
	private String title;
	private String id;
	private String image;

	public Album(String title, String id, String image) {
		this.title = title;
		this.id = id;
		this.image = image;
	}

	public String getTitle() {
		return title;
	}

	public String getId() {
		return id;
	}

	public String getImage() {
		return image;
	}
}
