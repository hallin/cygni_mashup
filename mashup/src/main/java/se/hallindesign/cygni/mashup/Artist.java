package se.hallindesign.cygni.mashup;

import java.util.List;

public class Artist {

	private String mbid;
	private String description;
	private List<Album> albums;

	public Artist(String mbid, String description, List<Album> albums) {
		this.mbid = mbid;
		this.description = description;
		this.albums = albums;
	}

	public String getMbid() {
		return mbid;
	}

	public String getDescription() {
		return description;
	}

	public List<Album> getAlbums() {
		return albums;
	}
}
