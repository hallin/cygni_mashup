package se.hallindesign.cygni.mashup.consumers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;

public class CovArtArchConsumer {

	private static final String URL_PREFIX = "http://coverartarchive.org/release-group/";
	private JsonNode responseNode;
	private static final Logger LOGGER = LoggerFactory.getLogger(CovArtArchConsumer.class);

	public CovArtArchConsumer(String id) {
		RestTemplate restTemplate = new RestTemplate();
		try {
			responseNode = restTemplate.getForObject(URL_PREFIX + id, JsonNode.class);
		} catch (RestClientException e) {
			LOGGER.error("only expected if id somehow is wrong", e);
			// responseNode will in this case be null since we couldn't get a cover
		}
		LOGGER.debug("JSON response = \n" + responseNode);
	}

	public String getImage() {
		if (responseNode == null) {
			LOGGER.debug("getImage() response is null, returning empty string");
			return "";
		}
		JsonNode image = responseNode.findPath("image");
		LOGGER.debug("getTitle() image: " + image);
		return image.asText();
	}

}
