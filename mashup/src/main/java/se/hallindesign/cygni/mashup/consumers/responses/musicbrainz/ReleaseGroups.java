package se.hallindesign.cygni.mashup.consumers.responses.musicbrainz;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReleaseGroups {

	private String title;
	private String id;
	@JsonProperty("primary-type")
	private String primaryType;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPrimaryType() {
		return primaryType;
	}

	public void setPrimaryType(String primaryType) {
		this.primaryType = primaryType;
	}
}
