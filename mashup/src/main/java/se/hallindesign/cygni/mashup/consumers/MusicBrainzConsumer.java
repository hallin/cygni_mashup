package se.hallindesign.cygni.mashup.consumers;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import se.hallindesign.cygni.mashup.consumers.responses.musicbrainz.MusicBrainzResponse;
import se.hallindesign.cygni.mashup.consumers.responses.musicbrainz.Relations;
import se.hallindesign.cygni.mashup.consumers.responses.musicbrainz.ReleaseGroups;
import se.hallindesign.cygni.mashup.consumers.responses.musicbrainz.URL;

public class MusicBrainzConsumer {

	private static final String URL_SUFFIX = "?&fmt=json&inc=url-rels+release-groups";
	private static final String URL_PREFIX = "http://musicbrainz.org/ws/2/artist/";
	private MusicBrainzResponse response;
	private static final Logger LOGGER = LoggerFactory.getLogger(MusicBrainzConsumer.class);

	public MusicBrainzConsumer(String mbid) {
		RestTemplate restTemplate = new RestTemplate();
		try {
			response = restTemplate.getForObject(URL_PREFIX + mbid + URL_SUFFIX, MusicBrainzResponse.class);
			/*
			 * TODO: Improve error handling for specific errors
			 */
		} catch (RestClientException e) {
			LOGGER.debug("Expected Error when wrong MBID is used", e);
			response = new MusicBrainzResponse();
		}
	}

	public String getWikipediaTitle() {
		List<Relations> relations = response.getRelations();
		Relations wikiReleation = relations.stream().filter(relation -> "wikipedia".equals(relation.getType()))
				.findAny().orElse(null);
		return wikiReleation == null ? "" : extractWikipediaTitle(wikiReleation.getUrl());
	}

	private String extractWikipediaTitle(URL url) {
		return url.getResource().substring("https://en.wikipedia.org/wiki/".length());
	}

	public String getWikidataId() {
		List<Relations> relations = response.getRelations();
		Relations wikiReleation = relations.stream().filter(relation -> "wikidata".equals(relation.getType())).findAny()
				.orElse(null);
		return wikiReleation == null ? "" : extractWikidataId(wikiReleation.getUrl());
	}

	private static String extractWikidataId(URL url) {
		return url.getResource().substring("https://www.wikidata.org/wiki/".length());
	}

	public List<ReleaseGroups> getAlbumReleaseGroups() {
		List<ReleaseGroups> releaseGroups = response.getReleaseGroups();
		LOGGER.debug("releaseGroups size = " + releaseGroups.size());
		List<ReleaseGroups> albums = releaseGroups.stream()
				.filter(releaseGroup -> "Album".equals(releaseGroup.getPrimaryType())).collect(Collectors.toList());
		return albums;
	}

}
