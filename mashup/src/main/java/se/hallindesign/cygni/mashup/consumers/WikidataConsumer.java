package se.hallindesign.cygni.mashup.consumers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;

public class WikidataConsumer {

	private static final String URL_PREFIX = "https://www.wikidata.org/w/api.php?action=wbgetentities&ids=";
	private static final String URL_SUFFIX = "&format=json&props=sitelinks";
	private JsonNode responseNode;

	private static final Logger LOGGER = LoggerFactory.getLogger(WikidataConsumer.class);

	public WikidataConsumer(String wikidataId) {
		RestTemplate restTemplate = new RestTemplate();
		responseNode = restTemplate.getForObject(URL_PREFIX + wikidataId + URL_SUFFIX, JsonNode.class);
		LOGGER.debug("JSON response = \n" + responseNode);
	}

	public String getTitle() {
		if (responseNode == null) {
			LOGGER.debug("getTitle() response is null, returning empty string");
			return "";
		}
		JsonNode siteLinks = responseNode.findPath("sitelinks");
		LOGGER.debug("getTitle() siteLinks: " + siteLinks);
		if (siteLinks.size() == 0) {
			return "";
		}
		JsonNode enwiki = siteLinks.get("enwiki");
		LOGGER.debug("getTitle() enwiki: " + enwiki);
		JsonNode titleJson = enwiki.get("title");
		LOGGER.debug("getTitle() titleJson: " + titleJson);
		String titleValue = titleJson.textValue();
		LOGGER.debug("getTitle() found title: " + titleValue);
		return titleValue;
	}

}
