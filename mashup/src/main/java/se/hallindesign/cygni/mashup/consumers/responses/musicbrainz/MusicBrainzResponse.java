package se.hallindesign.cygni.mashup.consumers.responses.musicbrainz;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MusicBrainzResponse {

	private String id;
	@JsonProperty("release-groups")
	private List<ReleaseGroups> releaseGroups = new ArrayList<>();
	private List<Relations> relations = new ArrayList<>();

	public MusicBrainzResponse() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<ReleaseGroups> getReleaseGroups() {
		return releaseGroups;
	}

	public void setReleaseGroups(List<ReleaseGroups> releaseGroups) {
		this.releaseGroups = releaseGroups;
	}

	public List<Relations> getRelations() {
		return relations;
	}

	public void setRelations(List<Relations> relations) {
		this.relations = relations;
	}
}
