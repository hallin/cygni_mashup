package se.hallindesign.cygni.mashup;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import se.hallindesign.cygni.mashup.consumers.CovArtArchConsumer;
import se.hallindesign.cygni.mashup.consumers.MusicBrainzConsumer;
import se.hallindesign.cygni.mashup.consumers.WikidataConsumer;
import se.hallindesign.cygni.mashup.consumers.WikipediaConsumer;
import se.hallindesign.cygni.mashup.consumers.responses.musicbrainz.ReleaseGroups;

@Service
public class ArtistService {

	public Artist getArtist(String mbid) {
		// TODO: in all Consumers, add caching
		MusicBrainzConsumer musicBrainzConsumer = new MusicBrainzConsumer(mbid);
		String title = musicBrainzConsumer.getWikipediaTitle();

		// TODO: Save time by building description and albums concurrently

		if (title.isEmpty()) {
			/*
			 * case when wikipedia page doesn't exist in musicBrainz, then we try to get
			 * title from wikidata instead
			 */
			String wikidataId = musicBrainzConsumer.getWikidataId();
			WikidataConsumer wikidataConsumer = new WikidataConsumer(wikidataId);
			title = wikidataConsumer.getTitle();
		}

		WikipediaConsumer wikipediaConsumer = new WikipediaConsumer(title);
		String description = wikipediaConsumer.getExtract();

		List<Album> albums = buildAlbums(musicBrainzConsumer.getAlbumReleaseGroups());

		return new Artist(mbid, description, albums);
	}

	private List<Album> buildAlbums(List<ReleaseGroups> albumReleaseGroups) {
		List<Album> albums = new ArrayList<>();
		for (ReleaseGroups releaseGroup : albumReleaseGroups) {
			String albumId = releaseGroup.getId();
			/*
			 * TODO: improve time by consuming image concurrently.
			 */
			CovArtArchConsumer covArtArchConsumer = new CovArtArchConsumer(albumId);
			String image = covArtArchConsumer.getImage();
			albums.add(new Album(releaseGroup.getTitle(), releaseGroup.getId(), image));
		}
		return albums;
	}

}
