package se.hallindesign.cygni.mashup.consumers.responses.musicbrainz;

public class URL {

	private String resource;

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}
}
