package se.hallindesign.cygni.mashup;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

public class ArtistServiceTest {

	@Test
	public void artistServiceReturnsArtistObject() {
		ArtistService artistService = new ArtistService();
		assertNotNull(artistService.getArtist("0123"));
	}

	@Test
	public void artistServiceReturnsArtistWithMbid() {
		String mbid = "0000";
		ArtistService artistService = new ArtistService();
		Artist artist = artistService.getArtist(mbid);
		assertEquals(mbid, artist.getMbid());
	}

	@Test
	public void artistServiceReturnsArtistWithDescription() {
		String mbid = "5b11f4ce-a62d-471e-81fc-a69a8278c7da";
		ArtistService artistService = new ArtistService();
		Artist artist = artistService.getArtist(mbid);
		String description = artist.getDescription();
		assertFalse(description.isEmpty());
	}

	@Test
	public void artistServiceReturnsArtistWithDescriptionWhenWikipediaExistInMusicBrainz() {
		String mbid = "d8df96ae-8fcf-4997-b3e6-e5d1aaf0f69e";
		ArtistService artistService = new ArtistService();
		Artist artist = artistService.getArtist(mbid);
		String description = artist.getDescription();
		assertFalse(description.isEmpty());
	}

	@Test
	public void artistServiceReturnsArtistWithAlbums() {
		String mbid = "5b11f4ce-a62d-471e-81fc-a69a8278c7da";
		ArtistService artistService = new ArtistService();
		Artist artist = artistService.getArtist(mbid);
		List<Album> albums = artist.getAlbums();
		assertFalse(albums.isEmpty());
		Album album = albums.get(0);
		assertFalse(album.getId().isEmpty());
		assertFalse(album.getTitle().isEmpty());
		assertFalse(album.getImage().isEmpty());
	}

}
