package se.hallindesign.cygni.mashup;

import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@WebMvcTest
public class ArtistControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ArtistService service;

	@Test
	public void assertResponseIsCorrectJson() throws Exception {
		String mbid = "123";
		Artist artist = createArtist(mbid);
		when(service.getArtist(mbid)).thenReturn(artist);
		// mockService(mbid);
		mockMvc.perform(get("/artist/" + mbid).contentType("application/json")).andExpect(status().isOk())
				.andExpect(jsonPath("$.mbid", is(artist.getMbid())))
				.andExpect(jsonPath("$.description", is(artist.getDescription())))
				.andExpect(jsonPath("$.albums[0].title", is(artist.getAlbums().get(0).getTitle())))
				.andExpect(jsonPath("$.albums[0].id", is(artist.getAlbums().get(0).getId())))
				.andExpect(jsonPath("$.albums[0].image", is(artist.getAlbums().get(0).getImage())))
				.andExpect(jsonPath("$.albums[1].title", is(artist.getAlbums().get(1).getTitle())))
				.andExpect(jsonPath("$.albums[1].id", is(artist.getAlbums().get(1).getId())))
				.andExpect(jsonPath("$.albums[1].image", is(artist.getAlbums().get(1).getImage())));
	}

	private Artist createArtist(String mbid) {
		List<Album> albums = new ArrayList<>();
		albums.add(new Album("some title", "123-456", "http://some.thing"));
		albums.add(new Album("some title2", "987-654", "http://foo.bar"));
		return new Artist(mbid, "desc", albums);
	}

}
