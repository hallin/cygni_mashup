package se.hallindesign.cygni.mashup.consumers;

import static org.junit.Assert.*;

import org.junit.Test;

public class WikidataConsumerTest {

	@Test
	public void titleIsReturnedForValidId() {
		WikidataConsumer wikidataConsumer = new WikidataConsumer("Q11649");
		String title = wikidataConsumer.getTitle();
		assertEquals("Nirvana (band)", title);
	}

	@Test
	public void nonValidWikidataIdResultsInEmptyString() {
		WikidataConsumer wikidataConsumer = new WikidataConsumer("Somethingwrong");
		String title = wikidataConsumer.getTitle();
		assertTrue(title.isEmpty());
	}

	@Test
	public void emptydWikidataIdResultsInEmptyString() {
		WikidataConsumer wikidataConsumer = new WikidataConsumer("");
		String title = wikidataConsumer.getTitle();
		assertTrue(title.isEmpty());
	}

}
