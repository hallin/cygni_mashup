package se.hallindesign.cygni.mashup.consumers;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import se.hallindesign.cygni.mashup.consumers.responses.musicbrainz.ReleaseGroups;

public class MusicBrainzConsumerTest {

	@Test
	public void wikipediaTitleIsFiltered() {
		String mbid = "d8df96ae-8fcf-4997-b3e6-e5d1aaf0f69e";
		MusicBrainzConsumer musicBrainzConsumer = new MusicBrainzConsumer(mbid );
		String wikipediaTitle = musicBrainzConsumer.getWikipediaTitle();
		assertTrue("The_Temptations".equals(wikipediaTitle));
	}

	@Test
	public void noValidMbidResultsInEmptyWikipediaTitle() {
		String mbid = "something-wrong";
		MusicBrainzConsumer musicBrainzConsumer = new MusicBrainzConsumer(mbid );
		String wikipediaTitle = musicBrainzConsumer.getWikipediaTitle();
		assertTrue(wikipediaTitle.isEmpty());
	}

	@Test
	public void emptyMbidResultsInEmptyWikipediaTitle() {
		MusicBrainzConsumer musicBrainzConsumer = new MusicBrainzConsumer("");
		String wikipediaTitle = musicBrainzConsumer.getWikipediaTitle();
		assertTrue(wikipediaTitle.isEmpty());
	}

	@Test
	public void nonExistingWikipediaTypeResultsInEmptyWikipediaTitle() {
		MusicBrainzConsumer musicBrainzConsumer = new MusicBrainzConsumer("5b11f4ce-a62d-471e-81fc-a69a8278c7da");
		String wikipediaTitle = musicBrainzConsumer.getWikipediaTitle();
		assertTrue(wikipediaTitle.isEmpty());
	}

	@Test
	public void wikidataIdIsFiltered() {
		String mbid = "5b11f4ce-a62d-471e-81fc-a69a8278c7da";
		MusicBrainzConsumer musicBrainzConsumer = new MusicBrainzConsumer(mbid );
		String wikidataId = musicBrainzConsumer.getWikidataId();
		assertTrue("Q11649".equals(wikidataId));
	}

	@Test
	public void noValidMbidResultsInEmptyWikidataId() {
		String mbid = "something-wrong";
		MusicBrainzConsumer musicBrainzConsumer = new MusicBrainzConsumer(mbid );
		String wikidataId = musicBrainzConsumer.getWikidataId();
		assertTrue(wikidataId.isEmpty());
	}

	@Test
	public void emptyMbidResultsInEmptyWikidataid() {
		MusicBrainzConsumer musicBrainzConsumer = new MusicBrainzConsumer("");
		String wikidataId = musicBrainzConsumer.getWikidataId();
		assertTrue(wikidataId.isEmpty());
	}

	@Test
	public void albumsIsFiltered() {
		String mbid = "5b11f4ce-a62d-471e-81fc-a69a8278c7da";
		MusicBrainzConsumer musicBrainzConsumer = new MusicBrainzConsumer(mbid );
		List<ReleaseGroups> albums = musicBrainzConsumer.getAlbumReleaseGroups();
		assertTrue(albums.size() > 0);
	}

}
