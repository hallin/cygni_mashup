package se.hallindesign.cygni.mashup.consumers;

import static org.junit.Assert.*;

import org.junit.Test;

public class CovArtArchConsumerTest {

	@Test
	public void imageIsReturnedForValidId() {
		CovArtArchConsumer covArtArchConsumer = new CovArtArchConsumer("1b022e01-4da6-387b-8658-8678046e4cef");
		String image = covArtArchConsumer.getImage();
		assertEquals("http://coverartarchive.org/release/a146429a-cedc-3ab0-9e41-1aaf5f6cdc2d/3012495605.jpg", image);
	}

	@Test
	public void nonValidIdResultsInEmptyString() {
		CovArtArchConsumer covArtArchConsumer = new CovArtArchConsumer("Somethingwrong");
		String image = covArtArchConsumer.getImage();
		assertTrue(image.isEmpty());
	}

	@Test
	public void emptydIdResultsInEmptyString() {
		CovArtArchConsumer covArtArchConsumer = new CovArtArchConsumer("");
		String image = covArtArchConsumer.getImage();
		assertTrue(image.isEmpty());
	}

}
