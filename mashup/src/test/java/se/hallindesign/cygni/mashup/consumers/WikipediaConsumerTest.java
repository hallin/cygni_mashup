package se.hallindesign.cygni.mashup.consumers;

import static org.junit.Assert.*;

import org.junit.Test;

public class WikipediaConsumerTest {

	@Test
	public void extractIsReturnedForArtistWithSpace() {
		WikipediaConsumer consumer = new WikipediaConsumer("Nirvana (band)");
		String extract = consumer.getExtract();
		assertTrue(extract.contains("<p><b>Nirvana</b> was an American rock band"));
	}

	@Test
	public void extractIsReturnedForArtistWithUnderscore() {
		WikipediaConsumer consumer = new WikipediaConsumer("Nirvana_(band)");
		String extract = consumer.getExtract();
		assertTrue(extract.contains("<p><b>Nirvana</b> was an American rock band"));
	}

	@Test
	public void nonValidWikidataIdResultsInEmptyString() {
		WikipediaConsumer consumer = new WikipediaConsumer("Somethingwrong");
		String extract = consumer.getExtract();
		assertTrue(extract.isEmpty());
	}

	@Test
	public void emptydWikidataIdResultsInEmptyString() {
		WikipediaConsumer consumer = new WikipediaConsumer("");
		String extract = consumer.getExtract();
		assertTrue(extract.isEmpty());
	}

}
