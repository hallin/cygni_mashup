package se.hallindesign.cygni.mashup;

import static org.junit.Assert.assertNotNull;
//import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.equalTo;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class MashupApplicationTests {

	@LocalServerPort
	private int port;

	@Autowired
	private ArtistController controller;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void contextLoads() {
		assertNotNull(controller);
	}

	@Test
	public void responseContainsMbid() {
		String mbid = "123";
		ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:" + port + "/artist/" + mbid,
				String.class);
		String body = response.getBody();
		System.out.println(body);
		assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
		assertTrue(body.contains("123"));
	}

}
