#Cygni Mashup

POC of a REST API To fetch Artist Data (mbid, description and albums). Internally builds the response by using four external REST API services: MusicBrainz, Wikidata, Wikipedia and Cover Art Archive.
Input parameter for the API is the artists MBID.
REST Endpoint: /artist/{mbid}
The REST API is build with Spring Boot.

## Build Package and Run Application
From folder mashup, build package:
```
$ ./mvnw clean install
```
run application from jar-file:
```
$ java -jar target/mashup-0.0.1-SNAPSHOT.jar
```

Test REST API by running request to <host>:<port>/artist/5b11f4ce-a62d-471e-81fc-a69a8278c7da with a REST consumer (for example CURL)
Example URL on localhost to get Nirvana: http://localhost:8080/artist/5b11f4ce-a62d-471e-81fc-a69a8278c7da

## Known issues and improvements
* Special error handling and specific Exceptions is limited in this POC
* Caching: Responses from external APIs are not Cached in this POC.
* Concurrent REST Consumers: The consumers are now sequential per request and can be improved to run concurrent.
